/*
 * ******************************************************************************
 *  Copyright Ⓒ 2016. TrinhQuan. All right reversed
 *  Author: TrinhQuan. Created on 2016/10/15
 *  Contact: trinhquan.171093@gmail.com
 * ******************************************************************************
 */

package com.tq.libs.common;

public interface Events {
    int BASE_EVENT = 1;

    int EVENT_SELECT = BASE_EVENT + 1;
    int EVENT_ADD = BASE_EVENT + 2;
}
